## WHAT IS GIT ?
Git is version-control software that makes collaboration with teammates super simple.

## Vocabulary used in GIT 
1.Repository
2.GitLab
3.Commit
4.Push
5.Branch
6.Merge
7.Clone
8.Fork
## Git Internals
